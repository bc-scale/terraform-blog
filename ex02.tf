# Use case 2: reserve an IP address with a device

# terraform plan -target=null_resource.ex02
# terraform apply -auto-approve -target=null_resource.ex02
# terraform.exe destroy -auto-approve -target=solidserver_device.p0015232l

resource "solidserver_device" "p0015232l" {
  name   = "p0015232l"
  class  = "SRVDC"
  class_parameters {
    serial = "AHCK42"
    form_factor = "R19-2U"
    model = "DELL-R840"
  }
}

# create an IP address for a future host linked to the existing space
resource "solidserver_ip_address" "SX01K03" {
  space   = "${solidserver_ip_space.blog.name}"
  # subnet  = "PAR_DC1"
  subnet  = "${solidserver_ip_subnet.PARDC1.name}"
  name    = "SX01K03"
  mac     = "62:b5:3d:ee:8f:56"
  device  = "${solidserver_device.p0015232l.name}"
}

output "SX01K03-ip" {
  value = "SX01K03: ${solidserver_ip_address.SX01K03.address}"
}

resource "null_resource" "ex02" {
  depends_on = [
                 "solidserver_ip_address.SX01K03"
               ]
}
